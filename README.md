# sekuraRN
Sekura React Native App

## Getting Started
* Clone sekuraRN
* Move to sekuraRN Directory
* - yarn install
* Move to sekuraRN/ios Directory
* - pod install
* Move back to sekuraRN Directory
* - react-native run-ios

## Built With
* [Textile](https://github.com/textileio/react-native-sdk) - Decentralized data wallet
* [Redux](https://github.com/reduxjs/react-redux) - Redux
* [React Navigation](https://github.com/react-navigation/react-navigation) - Moving between screens
* [Native Base](https://github.com/GeekyAnts/NativeBase) - UI components for React Native
* [QR Code Scanner](https://github.com/moaazsidat/react-native-qrcode-scanner) - QR Code Scanner Component for React Native

## Make compatible with AndroidX
* - yarn
* Sync gradle 
* - npx jetify
* - react-native run-android