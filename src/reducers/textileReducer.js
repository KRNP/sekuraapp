import * as types from '../actions/actionTypes';

export default function (state = { status: false, phrase: '', deviceListThreadId: '' }, action) {
    switch (action.type) {
        case types.SET_MNEMONIC_PHRASE:
            return { ...state, phrase: action.payload }
        case types.SET_DEVICE_LIST_THREAD_ID:
            return { ...state, deviceListThreadId: action.payload }
        case types.SET_FCM_TOKEN:
            return { ...state, fcmToken: action.payload }
        default:
            return state;
    }
}