import { applyMiddleware, createStore, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage' // defaults to localStorage for web and AsyncStorage for react-native
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'


import rootReducer from '../reducers'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['textileStatusReducer', 'activityReducer', 'asyncStatusReducer']
}

const logger = createLogger({
  collapsed: (getState, action, logEntry) => !logEntry.error,
  predicate: (getState, action) =>
    action && action.type !== 'SET_LINES'
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const persistedReducer = persistReducer(persistConfig, rootReducer)


let store = createStore(persistedReducer, {}, composeEnhancers(applyMiddleware(thunk, logger)))
let persistor = persistStore(store)

export default  {
  store: store,
  persistor: persistor
}