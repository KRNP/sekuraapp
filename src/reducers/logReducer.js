import {ADD_LOG, CLEAR_LOG} from '../actions/actionTypes';
let initialState = {
  logs: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_LOG: {
      return {
        logs: [action.payload.log, ...state.logs],
      };
    }
    case CLEAR_LOG: {
      return {
        logs: [],
      };
    }
    default:
      return state;
  }
};
