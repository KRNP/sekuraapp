/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { YellowBox } from 'react-native';
import './shim'
import { Text } from 'react-native';


Text.defaultProps = {
    ...Text.defaultProps,
    allowFontScaling: false
};

AppRegistry.registerComponent(appName, () => App);
YellowBox.ignoreWarnings(['Remote debugger']);