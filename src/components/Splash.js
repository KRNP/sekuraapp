import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as acts from '../actions/actionCreator';
import NavigationService from '../lib/NavigationService';
import DefaultPreference from 'react-native-default-preference';
import COLORS from '../resources/Colors';
import {gradient, ic_logo} from '../resources';
import {ImageBackground, View, Image, StyleSheet} from 'react-native';
import {intervalMethod, getLineNumber} from '../lib/utils';
class Spalsh extends React.Component {
  constructor(props) {
    super(props);
    this.changeScreen = this.changeScreen.bind(this);
  }

  componentDidMount() {
    // let lineNumber = getLineNumber();
    // let e = new Error(lineNumber);
    // console.log(e.stack.split('\n'));
    // setInterval(() => {
    //   //intervalMethod();
    // }, 5000);
    setTimeout(() => {
      this.changeScreen();
    }, 1000);
  }
  componentDidCatch(error, errorInfo) {
    console.log(error, errorInfo);
  }
  changeScreen() {
    DefaultPreference.get('agreed').then(function(value) {
      if (value == 'yes') {
        NavigationService.navigate('HomePage', {userName: 'Sekura'});
      } else {
        NavigationService.navigate('TermsPage', {userName: 'Sekura'});
      }
    });
  }

  render() {
    return (
      <View accessibilityLabel="container_splash" style={{flex: 1}}>
        <ImageBackground
          accessibilityLabel="bg_splash"
          style={styles.backgroundImage}
          source={gradient}>
          <Image
            accessibilityLabel="logo_splash"
            style={styles.icLogo}
            source={ic_logo}
          />
        </ImageBackground>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    acts: bindActionCreators(acts, dispatch),
  };
};

function mapStateToProps(state) {
  return {};
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: COLORS.colorPrimary,
    flexDirection: 'column',
  },

  backgroundImage: {
    flex: 1,
    resizeMode: 'stretch',
    padding: 50,
    justifyContent: 'center',
    flexDirection: 'column',
  },

  icLogo: {
    width: 170,
    height: 170,
    alignSelf: 'center',
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Spalsh);
