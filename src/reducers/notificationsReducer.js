import * as types from '../actions/actionTypes';

export default function (state = {valid: [], blocked: []}, action) {
    switch (action.type) {
        case types.UPDATE_NOTIFICATIONS_LIST:
            return {...action.payload}
        case types.BLOCK_NOTFICATION:
            return {...state, blocked: [...state.blocked, action.payload]}
        default:
            return state;
    }
}
