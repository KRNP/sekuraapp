import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as acts from '../actions/actionCreator';
import moment from 'moment';
import COLORS from '../resources/Colors';
import {NeomorphBox} from 'react-native-neomorph-shadows';
import {
  Dimensions,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  setState,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  ListItem,
  Title,
  View,
  Text,
  Button,
  Icon,
} from 'native-base';
import settings from '../lib/settings.json';

const {width, height} = Dimensions.get('window');

class DeviceDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filesToDisplay: [],
      isRefreshing: false,
      callDerivedState: true,
    };
  }

  componentDidMount() {
    this.loadFiles();
  }
  loadFiles = (offset = null) => {
    this.props.acts.getDeviceActivity(
      this.props.navigation.getParam('deviceAddress', ''),
      offset,
    );
  };
  static getDerivedStateFromProps(props, state) {
    if (state.callDerivedState) {
      return {
        filesToDisplay: Object.values(props.activityReducer)[0],
      };
    }
    return null;
  }

  filterFilesToDisplay = async fileType => {
    await this.setState({callDerivedState: false, filesToDisplay: []});
    if (fileType === 'all') {
      this.setState({
        filesToDisplay: [...Object.values(this.props.activityReducer)[0]],
      });
    } else if (fileType === 'locked') {
      for (const file of Object.values(this.props.activityReducer)[0]) {
        if (file.type == 'E') {
          this.setState({filesToDisplay: [...this.state.filesToDisplay, file]});
        }
      }
    } else if (fileType === 'unlocked') {
      for (const file of Object.values(this.props.activityReducer)[0]) {
        if (file.type == 'D') {
          this.setState({filesToDisplay: [...this.state.filesToDisplay, file]});
        }
      }
    }
  };

  pause() {
    let from = moment().format('X');
    let till = parseInt(from) + parseInt(settings.max_valid_notify_secs);

    this.props.acts.sendMessage(
      Object.keys(this.props.activityReducer)[0],
      this.props.navigation.getParam('notifyThreadId', ''),
      {from, till, action: 'LOCK'},
    );
  }

  disconnect() {
    let from = moment().format('X');
    let till = parseInt(from) + parseInt(settings.max_valid_notify_secs);

    this.props.acts.sendMessage(
      Object.keys(this.props.activityReducer)[0],
      this.props.navigation.getParam('notifyThreadId', ''),
      {from, till, action: 'DISCONNECT', status: 'INIT'},
    );
  }

  renderItem = ({item}) => {
    return (
      <ListItem
        accessibilityLabel="list_item_dd"
        key={item.now}
        style={styles.listItem}>
        {item.type == 'E' ? (
          <Icon name="lock" style={styles.headerFonts} />
        ) : (
          <Icon name="unlock" style={styles.headerFonts} />
        )}
        <Text style={styles.fileName} numberOfLines={1}>
          {item.file}{' '}
        </Text>
        <Text style={styles.timestamp} numberOfLines={1} right>
          {moment(item.now, 'X').format('h:mm A MMM D, YYYY')}{' '}
        </Text>
      </ListItem>
    );
  };
  loadMore = () => {
    if (this.state.isRefreshing) return null;
    let length = [...Object.values(this.props.activityReducer)[0]].length;
    let last_block = [...Object.values(this.props.activityReducer)[0]][
      length - 1
    ].block;
    this.loadFiles(last_block);
  };

  render() {
    const {navigate} = this.props.navigation;
    const {filesToDisplay} = this.state;
    return (
      <Container accessibilityLabel="container_dd" style={styles.background}>
        <Header accessibilityLabel="header_dd" style={styles.header}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}
              accessibilityLabel="device_detail_back_button">
              <Icon style={styles.headerFonts} name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerFonts}>
              {' '}
              {this.props.navigation.getParam('deviceName', '')}{' '}
            </Title>
          </Body>
          <Right />
        </Header>

        <View accessibilityLabel="device_info_dd" style={styles.rootView}>
          <View
            accessibilityLabel="file_count_dd"
            style={{flexDirection: 'column'}}>
            <Text style={styles.labelFilescount}>
              {' '}
              Number of Files Secured{' '}
            </Text>
            <Text style={styles.fileCount} accessibilityLabel="txt_files_count">
              {' '}
              {this.props.navigation.getParam('fileCount', '')}{' '}
            </Text>
          </View>

          <View
            accessibilityLabel="device_actions_dd"
            style={styles.viewButtons}>
            <TouchableOpacity
              accessibilityLabel="btn_pause_dd"
              onPress={() => this.pause()}>
              <NeomorphBox style={styles.pauseDevice}>
                <Icon name="pause" style={styles.actionIcons} />
                <Text style={styles.actionButtons}> Pause Device </Text>
              </NeomorphBox>
            </TouchableOpacity>

            <TouchableOpacity
              accessibilityLabel="btn_disconnect_dd"
              onPress={() => this.disconnect()}>
              <NeomorphBox style={styles.disconnectDevice}>
                <Icon name="square" style={styles.actionIcons} />
                <Text style={styles.actionButtons}> Disconnect </Text>
              </NeomorphBox>
            </TouchableOpacity>
          </View>
        </View>

        <Text accessibilityLabel="lbl_files_dd" style={styles.labelActivity}>
          {' '}
          Files Activity{' '}
        </Text>

        <View accessibilityLabel="files_filter_dd" style={styles.viewFilter}>
          <TouchableOpacity
            accessibilityLabel="btn_all_files_dd"
            onPress={async () => this.filterFilesToDisplay('all')}>
            <NeomorphBox style={styles.neumorph}>
              <Text style={styles.filterButtons}> All </Text>
            </NeomorphBox>
          </TouchableOpacity>

          <TouchableOpacity
            accessibilityLabel="btn_locked_files_dd"
            onPress={async () => this.filterFilesToDisplay('locked')}>
            <NeomorphBox style={styles.neumorph}>
              <Text style={styles.filterButtons}> Locked </Text>
            </NeomorphBox>
          </TouchableOpacity>

          <TouchableOpacity
            accessibilityLabel="btn_unlocked_files_dd"
            onPress={async () => this.filterFilesToDisplay('unlocked')}>
            <NeomorphBox style={styles.neumorph}>
              <Text style={styles.filterButtons}> Unlocked </Text>
            </NeomorphBox>
          </TouchableOpacity>
        </View>

        {Object.keys(this.props.activityReducer)[0] ==
        this.props.navigation.getParam('deviceAddress', '') ? (
          <FlatList
            onRefresh={this.loadFiles}
            accessibilityLabel="list_files_dd"
            style={styles.list}
            data={filesToDisplay}
            renderItem={this.renderItem}
            onEndReached={this.loadMore}
            onEndReachedThreshold={0.1}
            refreshing={this.state.isRefreshing}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            extraData={this.state.filesToDisplay}
          />
        ) : null}
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    acts: bindActionCreators(acts, dispatch),
  };
};

function mapStateToProps(state) {
  return {
    activityReducer: state.activityReducer,
  };
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: COLORS.colorPrimary,
    flexDirection: 'column',
  },

  header: {
    backgroundColor: COLORS.colorPrimary,
  },

  headerFonts: {
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
  },

  rootView: {
    width: width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginTop: 10,
    marginBottom: 15,
  },

  list: {
    flex: 1,
    padding: 10,
    marginTop: 10,
    marginBottom: 10,
  },

  listItem: {
    flexDirection: 'row',
    marginLeft: 0,
    paddingLeft: 10,
    justifyContent: 'flex-start',
  },

  fileName: {
    flex: 1,
    color: COLORS.fontColorPrimary,
    fontSize: 12,
    marginLeft: 10,
    fontFamily: 'ProductSans-Regular',
  },

  timestamp: {
    color: COLORS.fontColorPrimary,
    fontSize: 12,
    fontFamily: 'ProductSans-Regular',
  },

  neumorph: {
    shadowRadius: 3,
    borderRadius: 5,
    backgroundColor: COLORS.fontColorPrimary,
    width: 110,
    height: 40,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  labelFilescount: {
    color: COLORS.fontLight,
    fontSize: 14,
    fontFamily: 'ProductSans-Regular',
  },

  fileCount: {
    color: COLORS.fontColorPrimary,
    fontSize: 28,
    fontFamily: 'ProductSans-Bold',
  },

  labelActivity: {
    color: COLORS.fontLight,
    fontSize: 14,
    marginLeft: 20,
    fontFamily: 'ProductSans-Regular',
  },

  viewFilter: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 10,
    marginBottom: 10,
  },

  filterButtons: {
    color: COLORS.colorPrimary,
    fontSize: 16,
    fontFamily: 'ProductSans-Regular',
  },

  viewButtons: {
    flexDirection: 'column',
    alignSelf: 'flex-end',
    marginRight: 30,
  },

  actionIcons: {
    color: COLORS.fontColorPrimary,
    fontSize: 20,
  },

  actionButtons: {
    color: COLORS.fontColorPrimary,
    fontSize: 14,
    marginLeft: 10,
    fontFamily: 'ProductSans-Regular',
  },

  pauseDevice: {
    shadowRadius: 3,
    borderRadius: 5,
    backgroundColor: COLORS.colorPrimary,
    width: 150,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  disconnectDevice: {
    shadowRadius: 3,
    borderRadius: 5,
    backgroundColor: COLORS.red,
    width: 150,
    height: 40,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeviceDetail);
