import * as types from '../actions/actionTypes';

export default function (state = {}, action) {
    let newState = {}

    switch (action.type) {
        case types.ADD_DEVICE:
            return {... state, [action.payload.deviceId]: {...action.payload.details, deviceId: action.payload.deviceId}}
        case types.DELETE_DEVICE:
            
            for(idx in state) {
                if(idx != action.payload.idx) {
                    newState[idx] = state[idx]
                }
            }
            return newState
        case types.UPDATE_DEVICE_IDX:
            let deviceDetails = state[action.payload.deviceId]
            for(idx in state) {
                if(idx != action.payload.deviceId) {
                    newState[idx] = state[idx]
                }
            }
            return {...newState, [action.payload.details.deviceAddress]: {...deviceDetails, ...action.payload.details}}
        case types.UPDATE_DEVICE:
            if(state[action.payload.idx]) {
                return {...state, [action.payload.idx]: {...state[action.payload.idx], ...action.payload.details}}
            } else {
                return state
            }
        default:
            return state;
    }
}
