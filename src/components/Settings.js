import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as acts from '../actions/actionCreator';
import COLORS from '../resources/Colors';
import {NeomorphBox} from 'react-native-neomorph-shadows';
import {StyleSheet, Share, TouchableOpacity, setState} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Radio,
  ListItem,
  View,
  Text,
  Button,
  Icon,
} from 'native-base';
import * as config from '../lib/settings';

class Settings extends React.Component {
  constructor(props) {
    super(props);
  }

  onShare(recoveryKey) {
    console.log(recoveryKey);
    this.props.acts.log(recoveryKey);
    Share.share({
      message:
        'This is your Sekura Recovery Phrase, Keep it safe & handy :\n' +
        recoveryKey,
    });
  }

  render() {
    const {navigate} = this.props.navigation;

    return (
      <Container
        accessibilityLabel="container_settings"
        style={styles.background}>
        <Header accessibilityLabel="header_settings" style={styles.header}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon style={styles.headerFonts} name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerFonts}>Settings</Title>
          </Body>
          <Right />
        </Header>

        <TouchableOpacity
          accessibilityLabel="btn_recovery_key_settings"
          onPress={() => this.onShare(this.props.recoveryKey)}>
          <NeomorphBox style={styles.buttons}>
            <Icon style={styles.headerFonts} name="key" />
            <Text style={styles.buttonText}> Recovery Key </Text>
          </NeomorphBox>
        </TouchableOpacity>
        {config.showLogs && (
          <TouchableOpacity
            accessibilityLabel="btn_recovery_key_settings"
            onPress={() => this.props.navigation.navigate('LoggerScreen')}>
            <NeomorphBox style={styles.buttons}>
              <Icon style={styles.headerFonts} name="paper" />
              <Text style={styles.buttonText}> Logs </Text>
            </NeomorphBox>
          </TouchableOpacity>
        )}

        {/* <NeomorphBox
                    accessibilityLabel='btn_notification_prefs_settings'
                    style={styles.buttons} >
                    <Icon style={styles.headerFonts} name='notifications' />
                    <Text style={styles.buttonText} > Notification Preferences </Text>
                </NeomorphBox>

                <View accessibilityLabel='view_notification_options_settings' style={styles.options}>
                    <ListItem>
                        <Left>
                            <Text style={styles.radioButton}>Notification for each auth request</Text>
                        </Left>
                        <Right>
                            <Radio selected={true} />
                        </Right>
                    </ListItem>

                    <ListItem>
                        <Left>
                            <Text style={styles.radioButton}>Notification just onces a day</Text>
                        </Left>
                        <Right>
                            <Radio selected={false} />
                        </Right>
                    </ListItem>

                    <ListItem>
                        <Left>
                            <Text style={styles.radioButton}>Notification just once per session</Text>
                        </Left>
                        <Right>
                            <Radio selected={false} />
                        </Right>
                    </ListItem>
                </View> */}

        <TouchableOpacity accessibilityLabel="btn_recovery_key_settings">
          <NeomorphBox style={styles.buttons}>
            <Icon style={styles.headerFonts} name="bulb" />
            <Text style={styles.buttonText}> Theme </Text>
          </NeomorphBox>
        </TouchableOpacity>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    acts: bindActionCreators(acts, dispatch),
  };
};

function mapStateToProps(state) {
  return {
    recoveryKey: state.textileReducer.phrase,
  };
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: COLORS.colorPrimary,
    flexDirection: 'column',
  },

  header: {
    backgroundColor: COLORS.colorPrimary,
  },

  headerFonts: {
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
  },

  buttons: {
    shadowRadius: 3,
    backgroundColor: COLORS.colorPrimary,
    marginTop: 20,
    marginLeft: 20,
    paddingLeft: 20,
    width: 300,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
  },

  buttonText: {
    color: COLORS.fontColorPrimary,
    marginLeft: 10,
    fontFamily: 'ProductSans-Bold',
  },

  options: {
    marginLeft: 20,
    marginEnd: 20,
    marginTop: 5,
  },

  radioButton: {
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Settings);
