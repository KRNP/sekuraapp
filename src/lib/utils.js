import reduxSetup from './reduxSetup';
import * as types from '../actions/actionTypes';
import Textile from '@textile/react-native-sdk';
import RNFS from 'react-native-fs';
import * as config from './settings.json';
import moment from 'moment';
const FILENAME = 'UTILS';
const repoPath = `${RNFS.DocumentDirectoryPath}/${config.repoName}`;
const STORE = reduxSetup.store;
export const log = (...log) => {
  if (config.showLogs) {
    log = log.reduce((p, c) => p + ' ' + c, '');
    console.log(log);
    STORE.dispatch({
      type: types.ADD_LOG,
      payload: {
        log: {log: log, time: moment().format('X')},
      },
    });
  }
};
export const clearLog = () => {
  STORE.dispatch({
    type: types.CLEAR_LOG,
  });
};
export const intervalMethod = async () => {
  try {
    log(`${FILENAME}# 29 #---textile launch---`);
    await Textile.launch(repoPath, true).catch(err => log(err));
  } catch (err) {
    log(`${FILENAME}# 32 #${err}`);
  }
};
export const getLineNumber = () => {
  try {
    throw Error('');
  } catch (err) {
    return err;
  }
};
