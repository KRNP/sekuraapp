const COLORS = {
    colorPrimary: '#0088cc',
    colorAccent: '#0077aa',
    
    fontColorPrimary: 'white',
    fontLight: '#DCDCDC',
    fontDark: '#000',

    red: '#FF0000'
  }
  
export default COLORS;