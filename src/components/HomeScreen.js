import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as acts from '../actions/actionCreator';
import moment from 'moment';
import * as config from '../lib/settings';
import NavigationService from '../lib/NavigationService';
import {NeomorphBox} from 'react-native-neomorph-shadows';
import COLORS from '../resources/Colors';
import {
  ic_logo,
  ic_home,
  ic_plus,
  ic_settings,
  ic_laptop,
  connect_device_image,
} from '../resources';
import {
  Image,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  setState,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  List,
  ListItem,
  View,
  Text,
  FooterTab,
  Icon,
} from 'native-base';
import Toast from 'react-native-simple-toast';

const {width, height} = Dimensions.get('window');

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDeserted: true,
    };
  }

  async componentDidMount() {
    this.props.acts.launchTextile();
  }

  static getDerivedStateFromProps(props, state) {
    let devices = JSON.stringify(props.deviceList);
    if (devices.length > 2) {
      state.isDeserted = false;
    }
    return null;
  }

  openQRScreen() {
    if (config.release) {
      NavigationService.navigate('QRScreen', {userName: 'Sekura'});
    } else {
      NavigationService.navigate('QRTestScreen', {userName: 'Sekura'});
    }
  }
  onDeviceListPress = async (
    deviceAddress,
    deviceName,
    fileCount,
    threadID,
    deviceId,
    deviceLinked,
  ) => {
    // Toast.show(
    //   `${val.deviceAddress},${val.deviceName},${
    //     val.fileCount
    //   },${val.notifyThreadId},${val.deviceId}`,
    // );
    Toast.show(
      `device id = ${deviceId} clicked which is linked = ${deviceLinked}`,
    );
    if (deviceLinked) {
      this.props.navigation.navigate('DeviceDetailPage', {
        deviceAddress,
        deviceName,
        fileCount,
        notifyThreadId,
      });
    } else {
      return;
    }
  };
  render() {
    const {navigate} = this.props.navigation;

    return (
      <Container accessibilityLabel="container_home" style={styles.background}>
        <Header accessibilityLabel="header_home" style={styles.header}>
          <Left>
            <Image style={styles.ic_logo} source={ic_logo} />
          </Left>
          <Body>
            <Title style={styles.headerFonts}>Sekura</Title>
          </Body>
          <Right>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('LoggerScreen')}>
              <Icon style={styles.headerFonts} name="paper" />
            </TouchableOpacity>
            {/* {this.props.textileStatus ? null : <ActivityIndicator size="large" color="white" />} */}
          </Right>
        </Header>

        {this.state.isDeserted ? null : (
          <List accessibilityLabel="list_devices" style={styles.list}>
            {Object.entries(this.props.deviceList).map(([key, val], i) => {
              return (
                <ListItem
                  accessibilityLabel="listItem_devices"
                  noBorder
                  key={key}>
                  <TouchableOpacity
                    accessibilityLabel="btn_item_device"
                    // onPress={() => {
                    //   val.deviceLinked
                    //     ?
                    //     : null;
                    // }}
                    onPress={() => {
                      this.onDeviceListPress(
                        val.deviceAddress,
                        val.deviceName,
                        val.fileCount,
                        val.notifyThreadId,
                        val.deviceId,
                        val.deviceLinked,
                      );
                    }}>
                    <NeomorphBox style={styles.listItem}>
                      {val.deviceLinked ? (
                        <Image style={styles.listItemIcon} source={ic_laptop} />
                      ) : (
                        <ActivityIndicator size="small" color="white" />
                      )}

                      <View style={styles.listItemDeviceDetailView}>
                        <Text
                          style={styles.listItemDeviceName}
                          numberOfLines={1}>
                          {' '}
                          {val.deviceName}{' '}
                        </Text>
                        {val.deviceLinked ? (
                          <Text
                            style={styles.listItemDeviceLinkedStatus}
                            numberOfLines={1}
                            accessibilityLabel="device_associated">
                            {' '}
                            Associated on{' '}
                            {moment(val.linkedMoment, 'X').format(
                              ' MMMM D, YYYY',
                            )}
                          </Text>
                        ) : (
                          <Text
                            style={styles.listItemDeviceLinkedStatus}
                            numberOfLines={1}
                            accessibilityLabel="device_linked">
                            {' '}
                            Device is being linked...
                          </Text>
                        )}
                      </View>

                      <View style={styles.listItemDivider} />

                      <View style={styles.listItemFileCountView}>
                        <Text style={styles.listItemDeviceFileCountLabel}>
                          {' '}
                          Files Secured{' '}
                        </Text>
                        <Text style={styles.listItemDeviceFileCount}>
                          {' '}
                          {val.fileCount}{' '}
                        </Text>
                      </View>
                    </NeomorphBox>
                  </TouchableOpacity>
                </ListItem>
              );
            })}
          </List>
        )}

        {this.state.isDeserted ? (
          <View accessibilityLabel="idle_view_home" style={styles.viewDeserted}>
            <Image
              accessibilityLabel="illustration_home"
              style={styles.illustation}
              source={connect_device_image}
            />

            <Text
              accessibilityLabel="lbl_nodevices_home"
              style={styles.labelNoDevice}>
              {' '}
              There are no device connected at the moment{' '}
            </Text>

            <TouchableOpacity
              accessibilityLabel="btn_connect_device_home"
              onPress={() => this.openQRScreen()}>
              <NeomorphBox style={styles.buttonConnectDevice}>
                <Image style={styles.ic_logo} source={ic_laptop} />
                <Text style={styles.textConnectDevice}>
                  {' '}
                  Connect My Device{' '}
                </Text>
              </NeomorphBox>
            </TouchableOpacity>
          </View>
        ) : null}

        {this.state.isDeserted ? null : (
          <FooterTab
            accessibilityLabel="footer_home"
            style={styles.footerStyle}>
            <NeomorphBox
              accessibilityLabel="btn_home_home"
              inner
              style={styles.fabNeomorph}>
              <Image source={ic_home} style={styles.fab} />
            </NeomorphBox>

            <TouchableOpacity
              accessibilityLabel="btn_new_device_home"
              onPress={() => this.openQRScreen()}>
              <NeomorphBox style={styles.fabNeomorph}>
                <Image source={ic_plus} style={styles.fab} />
              </NeomorphBox>
            </TouchableOpacity>

            <TouchableOpacity
              accessibilityLabel="btn_settings_home"
              onPress={() => navigate('SettingsPage')}>
              <NeomorphBox style={styles.fabNeomorph}>
                <Image source={ic_settings} style={styles.fab} />
              </NeomorphBox>
            </TouchableOpacity>
          </FooterTab>
        )}
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    acts: bindActionCreators(acts, dispatch),
  };
};

function mapStateToProps(state) {
  return {
    textileStatus: state.textileStatusReducer,
    deviceList: state.deviceListReducer,
  };
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: COLORS.colorPrimary,
    flexDirection: 'column',
  },

  header: {
    backgroundColor: COLORS.colorPrimary,
  },

  headerFonts: {
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
  },

  list: {
    flex: 1,
    marginTop: 10,
  },

  viewDeserted: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },

  illustation: {
    width: width * 0.85,
    height: 300,
    alignSelf: 'center',
    marginBottom: 50,
  },

  labelNoDevice: {
    color: COLORS.fontColorPrimary,
    fontSize: 16,
    alignSelf: 'center',
    width: width * 0.9,
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: 'ProductSans-Regular',
  },

  buttonConnectDevice: {
    shadowRadius: 3,
    backgroundColor: COLORS.colorPrimary,
    width: width * 0.9,
    height: 70,
    marginBottom: 30,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  textConnectDevice: {
    color: COLORS.fontColorPrimary,
    fontSize: 22,
    marginLeft: 20,
    fontFamily: 'ProductSans-Regular',
  },

  listItem: {
    shadowRadius: 2,
    borderRadius: 10,
    backgroundColor: COLORS.colorPrimary,
    width: width * 0.92,
    height: 70,
    padding: 10,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  listItemIcon: {
    width: 30,
    height: 30,
    padding: 10,
  },

  listItemDeviceDetailView: {
    flex: 1,
    marginLeft: 10,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  listItemDeviceName: {
    color: COLORS.fontColorPrimary,
    fontSize: 18,
    fontFamily: 'ProductSans-Regular',
  },

  listItemDeviceLinkedStatus: {
    color: COLORS.fontColorPrimary,
    marginTop: 2,
    marginLeft: 3,
    fontSize: 12,
    fontFamily: 'ProductSans-Regular',
  },

  listItemDivider: {
    borderWidth: 0.5,
    height: 50,
    borderColor: COLORS.fontColorPrimary,
    margin: 10,
  },

  listItemFileCountView: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  listItemDeviceFileCountLabel: {
    color: COLORS.fontColorPrimary,
    fontSize: 10,
    fontFamily: 'ProductSans-Regular',
  },

  listItemDeviceFileCount: {
    color: COLORS.fontColorPrimary,
    fontSize: 20,
    fontFamily: 'ProductSans-Regular',
  },

  fab: {
    width: 30,
    height: 30,
    backgroundColor: COLORS.colorPrimary,
  },

  fabNeomorph: {
    shadowRadius: 2,
    borderRadius: 100,
    backgroundColor: COLORS.colorPrimary,
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },

  ic_logo: {
    width: 30,
    height: 30,
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    backgroundColor: COLORS.colorPrimary,
    bottom: 0,
    position: 'absolute',
    marginBottom: 20,
    width: width,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);
