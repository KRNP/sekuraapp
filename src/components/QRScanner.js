import React from 'react';

import {
  Image,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as acts from '../actions/actionCreator';
import QRCodeScanner from 'react-native-qrcode-scanner';
import RNCamera from 'react-native-camera';
import * as Animatable from 'react-native-animatable';
import COLORS from '../resources/Colors';

import {ic_logo} from '../resources';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  List,
  ListItem,
  View,
  Text,
  FooterTab,
  Icon,
  Button,
} from 'native-base';
import Toast from 'react-native-simple-toast';
import {log} from '../lib/utils';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

console.disableYellowBox = true;

const overlayColor = 'rgba(0,0,0,0.5)'; // this gives us a black color with a 50% transparency

const rectDimensions = SCREEN_WIDTH * 0.65; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = 'gray';

const scanBarWidth = SCREEN_WIDTH * 0.56; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.005; //this is equivalent to 1 from a 393 device width
const scanBarColor = COLORS.colorPrimary;

const iconScanColor = 'blue';

const FILENAME = 'QRSCANNER'

class QRScanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceDetails: null,
      isLoading: false,
    };
    this.onSuccess = this.onSuccess.bind(this);
  }

  async onSuccess(qrcode) {
    Toast.show("QR Code success called");
    let qrData = JSON.parse(qrcode.data);
    if (qrData) {
      await this.setState({deviceDetails: qrData});
      log(`${FILENAME}# 70 #${JSON.stringify(qrData)}`);
      let desktopPeerAddress = qrData.d_public_key;
      let desktopDeviceId = qrData.d_device_id;
      let desktopDeviceName = qrData.d_device_name;
      let numOfNodes = qrData.num_nodes;
      Toast.show("Adding device");
      this.props.acts.addDevice(
        desktopDeviceId,
        desktopDeviceName,
        desktopPeerAddress,
        numOfNodes,
      );
    }
  }

  async UNSAFE_componentWillReceiveProps(nextProps) {
    const {deviceDetails} = this.state;
    const {deviceList} = nextProps;
    if (deviceList) {
      Object.keys(deviceList).map(async item => {
        if (deviceList[item].deviceId === deviceDetails.d_device_id) {
          log(`${FILENAME}# 91 #device found`);
          Toast.show("Device found");
          await this.setState({
            isLoading: false,
          });
          this.props.navigation.goBack();
        }
      });
    }
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_HEIGHT * 0.15,
      },
      to: {
        [translationType]: fromValue,
      },
    };
  }

  render() {
    const {navigate} = this.props.navigation;
    const {isLoading} = this.state;
    return (
      <Container accessibilityLabel="container_home" style={styles.background}>
        <Header accessibilityLabel="header_home" style={styles.header}>
          <Left>
            <Image style={styles.ic_logo} source={ic_logo} />
          </Left>
          <Body>
            <Title style={styles.headerFonts}>Sekura</Title>
          </Body>
          <Right>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('LoggerScreen')}>
              <Icon style={styles.headerFonts} name="paper" />
            </TouchableOpacity>
            {/* {this.props.textileStatus ? null : <ActivityIndicator size="large" color="white" />} */}
          </Right>
        </Header>
        {!isLoading && (
          <QRCodeScanner
            vibrate={false}
            accessibilityLabel="qrview_qr"
            showMarker
            onRead={this.onSuccess}
            cameraStyle={{height: SCREEN_HEIGHT * 1.05}}
            customMarker={
              <View
                accessibilityLabel="main_view_qr"
                style={styles.rectangleContainer}>
                <Button
                  accessibilityLabel="btn_goback_qr"
                  transparent
                  style={styles.backbutton}
                  onPress={() => this.props.navigation.goBack()}>
                  <Icon
                    style={{color: 'white', backgroundColor: 'transparent'}}
                    name="arrow-back"
                  />
                </Button>

                <View
                  accessibilityLabel="lbl_info_qr"
                  style={styles.topOverlay}>
                  <Text style={styles.scanMessage}>
                    Scan QR Code on Sekura Desktop
                  </Text>
                </View>

                <View style={{flexDirection: 'row'}}>
                  <View style={styles.leftAndRightOverlay} />

                  <View
                    accessibilityLabel="animation_view_qr"
                    style={styles.rectangle}>
                    <Icon
                      name="ios-qr-scanner"
                      size={SCREEN_WIDTH * 0.73}
                      color={iconScanColor}
                    />
                    <Animatable.View
                      style={styles.scanBar}
                      direction="alternate-reverse"
                      iterationCount="infinite"
                      duration={5000}
                      easing="linear"
                      animation={this.makeSlideOutTranslation(
                        'translateY',
                        SCREEN_WIDTH * -0.35,
                      )}
                    />
                  </View>

                  <View style={styles.leftAndRightOverlay} />
                </View>

                <View style={styles.bottomOverlay} />
              </View>
            }
          />
        )}
        {isLoading && (
          <View
            style={{
              position: 'absolute',
              top: 70,
              left: 0,
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              width: '100%',
              height: '100%',
              backgroundColor: 'transparent',
            }}>
            {/* {this.state.loggerMessage &&
              this.state.loggerMessage.length !== 0 &&
              this.state.loggerMessage.map(item => {
                <Text
                  style={{textAlign: 'center', color: 'white', marginTop: 10}}>
                  {item}
                </Text>;
              })} */}

            <ActivityIndicator size="large" color="white" />
          </View>
        )}
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    acts: bindActionCreators(acts, dispatch),
  };
};

function mapStateToProps(state) {
  return {
    textileStatus: state.textileStatusReducer,
    deviceList: state.deviceListReducer,
    loader: state.asyncStatusReducer,
  };
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: COLORS.colorPrimary,
    flexDirection: 'column',
  },

  header: {
    backgroundColor: COLORS.colorPrimary,
    height: 70,
  },

  headerFonts: {
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
  },
  backbutton: {
    backgroundColor: overlayColor,
    width: SCREEN_WIDTH,
  },

  scanMessage: {
    fontSize: 20,
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
  },

  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: 'center',
    alignItems: 'center',
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25,
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor,
  },
  ic_logo: {
    width: 30,
    height: 30,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QRScanner);
