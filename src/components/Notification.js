import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as acts from '../actions/actionCreator';
import moment from 'moment';
import COLORS from '../resources/Colors';
import {NeomorphBox} from 'react-native-neomorph-shadows';
import {ic_logo} from '../resources';
import {
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  setState,
} from 'react-native';
import {Container, View, Text, Icon} from 'native-base';

const {width, height} = Dimensions.get('window');

class Notification extends React.Component {
  constructor(props) {
    super(props);
  }

  sendNotificationResponse(response) {
    this.props.acts.sendNotificationResponse(
      this.props.notifications[0].notifyThreadId,
      this.props.notifications[0].msgBlock,
      this.props.notifications[0].msg,
      response,
    );

    this.props.navigation.goBack();
  }

  render() {
    const {navigate} = this.props.navigation;

    return (
      <Container
        accessibilityLabel="container_notification"
        style={styles.background}>
        {this.props.notifications.length > 0 ? (
          <View
            accessibilityLabel="view_main_notification"
            style={styles.rootView}>
            <Image
              accessibilityLabel="logo_notification"
              style={styles.icLogo}
              source={ic_logo}
            />

            <Text
              accessibilityLabel="lbl_action_notification"
              style={styles.textTitle}>
              {' '}
              Action :
            </Text>
            <Text
              accessibilityLabel="action_notification"
              style={styles.textContent}>
              {' '}
              {this.props.notifications[0].msg.action}{' '}
            </Text>
            <Text
              accessibilityLabel="lbl_devicename_notification"
              style={styles.textTitle}>
              {' '}
              Device Name :
            </Text>
            <Text
              accessibilityLabel="devicename_notification"
              style={styles.textContent}>
              {' '}
              {this.props.notifications[0].deviceName}{' '}
            </Text>
            <Text
              accessibilityLabel="lbl_datetime_notification"
              style={styles.textTitle}>
              {' '}
              Time Requested :
            </Text>
            <Text
              accessibilityLabel="time_notification"
              style={styles.textContent}>
              {' '}
              {moment(this.props.notifications[0].msg.from, 'X').format(
                'h:mm:ss a',
              )}{' '}
            </Text>
            <Text
              accessibilityLabel="date_notification"
              style={styles.textContent}>
              {' '}
              {moment(this.props.notifications[0].msg.from, 'X').format(
                'MMMM D, YYYY',
              )}{' '}
            </Text>

            {this.props.notifications[0].msg.action == 'DECRYPT' ? (
              <View accessibilityLabel="view_filename_notification">
                <Text
                  accessibilityLabel="lbl_filename_notification"
                  style={styles.textTitle}>
                  {' '}
                  File Name :
                </Text>
                <Text
                  accessibilityLabel="filename_notification"
                  style={styles.textContent}>
                  {' '}
                  {this.props.notifications[0].msg.file}{' '}
                </Text>
              </View>
            ) : null}

            <View
              accessibilityLabel="actions_notification"
              style={styles.buttonsView}>
              <TouchableOpacity
                accessibilityLabel="btn_approve_notification"
                onPress={() => this.sendNotificationResponse('approved')}>
                <NeomorphBox style={styles.neomorph}>
                  <Icon name="home" style={styles.iconButtons} />
                  <Text style={styles.textContent}> Approve </Text>
                </NeomorphBox>
              </TouchableOpacity>
              <TouchableOpacity
                accessibilityLabel="btn_approve_now_notification"
                onPress={() => this.sendNotificationResponse('approved')}>
                <NeomorphBox style={styles.neomorph_now}>
                  <Icon name="home" style={styles.iconButtons} />
                  <Text style={styles.textContent}> Approve for now </Text>
                </NeomorphBox>
              </TouchableOpacity>
            </View>
            <View
              accessibilityLabel="actions_notification"
              style={styles.buttonsView}>
              <TouchableOpacity
                accessibilityLabel="btn_approve_notification"
                onPress={() => this.sendNotificationResponse('approved')}>
                <NeomorphBox style={styles.neomorph_hours}>
                  <Icon name="home" style={styles.iconButtons} />
                  <Text style={styles.textContent}> Approve for 24 hours</Text>
                </NeomorphBox>
              </TouchableOpacity>
              <TouchableOpacity
                accessibilityLabel="btn_deny_notification"
                onPress={() => this.sendNotificationResponse('rejected')}>
                <NeomorphBox style={styles.neomorph}>
                  <Icon name="trash" style={styles.iconButtons} />
                  <Text style={styles.textContent}> Deny </Text>
                </NeomorphBox>
              </TouchableOpacity>
            </View>
          </View>
        ) : null}
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    acts: bindActionCreators(acts, dispatch),
  };
};

function mapStateToProps(state) {
  return {
    notifications: state.notificationsReducer.valid.filter(validNote =>
      state.notificationsReducer.blocked.every(
        blockedNote => validNote.msgBlock != blockedNote,
      ),
    ),
  };
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: COLORS.colorPrimary,
    flexDirection: 'column',
  },

  rootView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },

  buttonsView: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: width,
    marginTop: 40,
  },

  icLogo: {
    width: 90,
    height: 90,
    alignSelf: 'center',
    marginBottom: 10,
  },

  textTitle: {
    alignSelf: 'center',
    color: COLORS.fontLight,
    fontSize: 14,
    marginTop: 20,
    fontFamily: 'ProductSans-Regular',
  },

  textContent: {
    alignSelf: 'center',
    color: COLORS.fontColorPrimary,
    fontSize: 18,
    marginTop: 1,
    fontFamily: 'ProductSans-Bold',
  },

  neomorph: {
    flexDirection: 'row',
    shadowRadius: 2,
    borderRadius: 5,
    backgroundColor: COLORS.colorPrimary,
    width: 150,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  neomorph_now: {
    flexDirection: 'row',
    shadowRadius: 2,
    borderRadius: 5,
    backgroundColor: COLORS.colorPrimary,
    width: 200,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  neomorph_hours: {
    flexDirection: 'row',
    shadowRadius: 2,
    borderRadius: 5,
    backgroundColor: COLORS.colorPrimary,
    width: 230,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },

  iconButtons: {
    marginRight: 5,
    color: COLORS.fontColorPrimary,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification);
