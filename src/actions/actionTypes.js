export const SET_TEXTILE_STATUS ='SET_TEXTILE_STATUS';
export const SET_MNEMONIC_PHRASE ='SET_MNEMONIC_PHRASE';
export const SET_DEVICE_LIST_THREAD_ID ='SET_DEVICE_LIST_THREAD_ID';
export const ADD_DEVICE ='ADD_DEVICE';
export const DELETE_DEVICE ='DELETE_DEVICE';
export const UPDATE_DEVICE ='UPDATE_DEVICE';
export const UPDATE_DEVICE_IDX ='UPDATE_DEVICE_IDX';
export const SET_FCM_TOKEN ='SET_FCM_TOKEN';
export const RESET_LOG = 'RESET_LOG'
export const UPDATE_LOG = 'UPDATE_LOG'
export const ASYNC_BUSY = 'ASYNC_BUSY'
export const ASYNC_DONE = 'ASYNC_DONE'
export const UPDATE_NOTIFICATIONS_LIST = 'UPDATE_NOTIFICATIONS_LIST'
export const BLOCK_NOTFICATION = 'BLOCK_NOTFICATION'
export const ADD_LOG = 'ADD_LOG'
export const CLEAR_LOG = 'CLEAR_LOG'