import React, {Component} from 'react';
import {FlatList, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Radio,
  ListItem,
  View,
  Text,
  Button,
  Icon,
} from 'native-base';
import COLORS from '../resources/Colors';
import moment from 'moment';
import {clearLog} from '../lib/utils';
class Logger extends Component {
  constructor(props) {
    super(props);
  }
  renderItem = item => {
    let data = item.item;
    return (
      <View style={styles.logCard}>
        <Text style={styles.logText}>
          {moment.unix(data.time).format('DD-MM-YYYY H:mm:s')}
        </Text>
        <Text style={styles.logText}>{data.log}</Text>
      </View>
    );
  };
  clearLogs = () => {
    clearLog();
  };
  render() {
    const {logs} = this.props;
    return (
      <Container
        accessibilityLabel="container_settings"
        style={styles.background}>
        <Header accessibilityLabel="header_settings" style={styles.header}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon style={styles.headerFonts} name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerFonts}>Logger</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.clearLogs}>
              <Icon style={styles.headerFonts} name="trash" />
            </Button>
          </Right>
        </Header>
        <FlatList
          style={{marginTop: 10}}
          data={logs}
          renderItem={this.renderItem}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          extraData={this.props.logs}
        />
      </Container>
    );
  }
}
const mapStateToProps = state => {
  const {
    logReducer: {logs},
  } = state;
  return {logs};
};
export default connect(
  mapStateToProps,
  null,
)(Logger);

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: COLORS.fontDark,
    flexDirection: 'column',
  },

  header: {
    backgroundColor: COLORS.colorPrimary,
  },

  headerFonts: {
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
  },
  logCard: {
    marginVertical: 5,
    width: '90%',
    padding: 10,
    alignSelf: 'center',
  },
  logText: {
    color: COLORS.fontColorPrimary,
    fontFamily: 'ProductSans-Regular',
    fontSize: 14,
  },
});
