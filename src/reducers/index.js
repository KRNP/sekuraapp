import { combineReducers } from 'redux';
import deviceListReducer from './deviceListReducer'
import textileReducer from './textileReducer'
import textileStatusReducer from './textileStatusReducer'
import activityReducer from './activityReducer'
import asyncStatusReducer from './asyncStatusReducer'
import notificationsReducer from './notificationsReducer'
import logReducer from './logReducer'

const rootReducers = combineReducers({
    deviceListReducer,
    textileReducer,
    textileStatusReducer,
    activityReducer,
    asyncStatusReducer,
    notificationsReducer,
    logReducer
})

export default rootReducers;
