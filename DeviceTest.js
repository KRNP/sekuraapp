import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as acts from './src/actions/actionCreator'
import moment from 'moment';

const styles = StyleSheet.create({
  
  enabledButton: {
    alignItems: 'center',
    backgroundColor: 'blue',
    padding: 10
  },

  disabledButton: {
    alignItems: 'center',
    backgroundColor: 'grey',
    padding: 10
  }
})

class DeviceTest extends React.Component {

  constructor(props) {
    super(props)
    this.getTextileStatus = this.getTextileStatus.bind(this)
    this.addDevice = this.addDevice.bind(this)
    this.getButtonStyles = this.getButtonStyles.bind(this)
    this.state = {fcmToken: ''}
  }

  // async fcm () {
  //   await firebase.messaging().registerForRemoteNotifications();
  //   const fcmToken = await firebase.messaging().getToken();
  //   console.log("fcmToken:", fcmToken)
  //   this.setState({fcmToken})

  //   firebase.messaging().onMessage(async (remoteMessage) => {
  //     console.log('FCM Message Data:', remoteMessage.data);
   
  //  });
  // }

  // async sendMsg () {
  //   await firebase.messaging().sendMessage('from mobile')
  // }

  componentDidMount() {
    this.props.acts.launchTextile()
    // this.fcm()
  }

  componentWillUnmount() {
    // this.props.acts.setTextileStatus(false)
  }

  addDevice = () => {
    if(this.props.textileStatus) {
      this.props.acts.addDevice('61868e54fa8a43f0934b25416044156a882faa7e', "Dominic’s MacBook Pro", 'P8P67wfHmc2qQpaczCGefnE3xSynPuYw5vvgyWjA1bSjcqFe', 1)
    }
    // this.sendMsg()
  }

  getTextileStatus() {
    if(this.props.textileStatus) {
      return 'Online'
    }
    return 'Offline'
  }

  getButtonStyles() {
    if(this.props.textileStatus) {
      return styles.enabledButton
    }
    return styles.disabledButton
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Hello, world!</Text>
        <View style={{height: 100}}></View>
        <TouchableOpacity
         style={this.getButtonStyles()} disabled={!this.props.textileStatus} onPress={this.addDevice}
       >
       {/* <TouchableOpacity style={styles.enabledButton} onPress={this.addDevice}
       > */}
         <Text style={{color: '#DDDDDD'}}> Add Device - {this.getTextileStatus()} </Text>
       </TouchableOpacity>
      </View>
    );
  }
}


const mapDispatchToProps = dispatch => {
  return {
      acts: bindActionCreators(acts, dispatch)
  }
};

function mapStateToProps(state) {
  console.log('****^^^^*****')
  for(let d in state.deviceListReducer) {
    console.log('fileCount:', state.deviceListReducer[d].fileCount)
    console.log('Linked date and time:', moment(state.deviceListReducer[d].linkedMoment, 'X').format('MMMM Do YYYY, h:mm:ss a'))
    console.log(state.deviceListReducer[d])
  }
  return {
    textileStatus: state.textileStatusReducer
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DeviceTest);