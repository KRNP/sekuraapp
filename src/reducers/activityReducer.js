import * as types from '../actions/actionTypes';

export default function (state = {}, action) {
    switch (action.type) {
        case types.RESET_LOG:
            return {...state, [action.payload.idx]: [...action.payload.activity]}
        case types.UPDATE_LOG:
            return {...state, [action.payload.idx]: [...state[action.payload.idx], ...action.payload.activity]}
        default:
            return state;
    }
}
