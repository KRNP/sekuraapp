import * as types from '../actions/actionTypes';
import Textile, {
  Thread,
  AddThreadConfig,
  QueryOptions,
  ContactQuery,
  IContact,
  TextList,
  FeedItemType,
} from '@textile/react-native-sdk';
import RNFS from 'react-native-fs';
import * as config from '../lib/settings';
import reduxSetup from '../lib/reduxSetup';
import {EventEmitter} from 'events';
import {firebase} from '@react-native-firebase/messaging';
import moment from 'moment';
import NavigationService from '../lib/NavigationService';
import settings from '../lib/settings.json';
import {log} from '../lib/utils';

let emitter = new EventEmitter();

let FILENAME = 'ACTION CREATOR';

const repoPath = `${RNFS.DocumentDirectoryPath}/${config.repoName}`;
const STORE = reduxSetup.store;

emitter.on('KeyStore', async eventData => {
  let peer = eventData.feedItem.value.user.address;
  let threadId = eventData.threadId;

  let files = await Textile.files.list(threadId, '', 100000).catch(err => {
    log(`${FILENAME}# 33 #  ${err} `);
  });

  let uniqueFiles = new Set();

  files.items.map(f => uniqueFiles.add(f.caption));

  STORE.dispatch({
    type: types.UPDATE_DEVICE,
    payload: {idx: peer, details: {fileCount: uniqueFiles.size}},
  });
});

Textile.events.addThreadUpdateReceivedListener(async (threadId, feedItem) => {
  log(
    `${FILENAME}# 47 # Thread update event:, ${threadId}, ${JSON.stringify(
      feedItem,
    )}`,
  );
  let currentAddress = await Textile.account.address().catch(err => {
    log(`${FILENAME}# 49 #${err} `);
  });

  if (feedItem.value.user.address == currentAddress) {
    return;
  }
  let myThread = await Textile.threads.get(threadId).catch(err => {
    log(`${FILENAME}# 56 #${err} `);
  });

  if (
    !!myThread &&
    myThread.name == 'Notify' &&
    feedItem.type == FeedItemType.Text
  ) {
    updateNotifications().catch(err => {
      log(`${FILENAME}# 65 #${err} `);
    });
  } else if (!!myThread && myThread.name == 'KeyStore') {
    emitter.emit('KeyStore', {threadId, feedItem});
  } else {
    emitter.emit(threadId, feedItem);
  }
});

Textile.events.addNotificationReceivedListener(note => {
  log(`${FILENAME}# 79 #Notification Received:, ${JSON.stringify(note)}`);
  if (note.body == 'invited you to join') {
    Textile.invites.accept(note.block).then(async success => {
      if (note.subjectDesc.startsWith('KeyStore')) {
        let address = note.user.address;
        STORE.dispatch({
          type: types.UPDATE_DEVICE,
          payload: {idx: address, details: {keyStoreThreadId: note.subject}},
        });
        Textile.messages.add(note.subject, JSON.stringify({ack: true}));
        log(`${FILENAME}# 89 #....Ack Key Store....`);
      }
      if (note.subjectDesc.startsWith('Notify')) {
        let address = note.user.address;
        STORE.dispatch({
          type: types.UPDATE_DEVICE,
          payload: {idx: address, details: {notifyThreadId: note.subject}},
        });
        Textile.messages.add(note.subject, JSON.stringify({ack: true}));
        log(`${FILENAME}# 98 #....Ack Notify....`);
      }
    });
  }
});

Textile.events.addThreadAddedListener(threadId => {
  log(`${FILENAME}# 105 #'new Thread added: ' ${threadId}`);
});

Textile.events.addNodeStartedListener(async () => {
  log(`${FILENAME}# 109 #**node**: Node online`);
  log(`${FILENAME}# 110 #node address: ${await Textile.account.address()}`);

  log(`${FILENAME}# 112 #---textile sync---`);
  await Textile.account
    .sync({remoteOnly: true})
    .catch(err => log(`${FILENAME}# 115 #${err}`));

  let profile = await Textile.profile
    .get()
    .catch(err => log(`${FILENAME}# 119 #${err}`));

  if (!profile) {
    log(`${FILENAME}# 122 #NO PROFILE DETAILS, EXITING....`);
    return;
  }
  log(`${FILENAME}# 125 #node peer id:${profile.id}`);
  log(
    `${FILENAME}# 127 #Cafe register status ${await Textile.cafes
      .register(config.cafe, config.token)
      .catch(err => log(err))}`,
  ),
    log(`${FILENAME}# 131 #${await Textile.cafes.sessions()}`);

  await createDeviceListThread().catch(err => log(`${FILENAME}# 132 #${err}`));
  updateNotifications().catch(err => log(`${FILENAME}# 134 #${err}`));
  STORE.dispatch({type: types.SET_TEXTILE_STATUS, payload: true});
});

Textile.events.addNodeStoppedListener(() => {
  log(`${FILENAME}# 139 #**node**: Node stopped`);
  STORE.dispatch({type: types.SET_TEXTILE_STATUS, payload: false});
  Textile.launch(repoPath, true);
});

Textile.events.addContactQueryResultListener(async (queryId, contact) => {
  await Textile.contacts
    .add(contact)
    .catch(err => log(`${FILENAME}# 147 #add contact err ${err}`));

  let contactList = await Textile.contacts
    .list()
    .catch(err => log(`${FILENAME}# 151 #${err}`));
  log(`${FILENAME}# 152 #Contacts count: ${contactList.items.length}`);
  contactList.items.map(c => {
    log(`${FILENAME}# 154 #Contacts - ${c}`);
  });
  emitter.emit(queryId);
});

/********/

let initAccount = async dispatch => {
  let status = await Textile.isInitialized(repoPath).catch(err =>
    log(`${FILENAME}# 163 #${err}`),
  );

  if (!status) {
    let phrase = await Textile.initializeCreatingNewWalletAndAccount(
      repoPath,
      false,
      false,
    ).catch(err => log(`${FILENAME}# 171 #${err}`));
    log(`${FILENAME}# 172 #Phrase - ${phrase}`);
    dispatch({type: types.SET_MNEMONIC_PHRASE, payload: phrase});
    await firebase
      .messaging()
      .registerForRemoteNotifications()
      .catch(err => log(`${FILENAME}# 177 #${err}`));
    let fcmToken = await firebase.messaging().getToken();
    dispatch({type: types.SET_FCM_TOKEN, payload: fcmToken});
  }
};

let createThread = async key => {
  const threadKey = key;
  const threadName = key;
  const threads = await Textile.threads
    .list()
    .catch(err => log(`${FILENAME}# 188 #${err}`));
  var myThread =
    threads &&
    threads.items &&
    threads.items.find(thread => thread.key === threadKey);

  if (!myThread) {
    const config = {
      key: threadKey,
      name: threadName,
      type: Thread.Type.OPEN,
      sharing: Thread.Sharing.INVITE_ONLY,
      schema: {id: '', json: '', preset: AddThreadConfig.Schema.Preset.BLOB},
      force: false,
      whitelist: [],
    };

    myThread = await Textile.threads
      .add(config)
      .catch(err => log(`${FILENAME}# 207 #${err}`));
  }

  return myThread.id;
};

let createDeviceListThread = async () => {
  let threadId = await createThread('DEVICE_LIST').catch(err =>
    log(`${FILENAME}# 215 #${err}`),
  );
  STORE.dispatch({type: types.SET_DEVICE_LIST_THREAD_ID, payload: threadId});
};

let createHandshakeThread = async deviceId => {
  let threadId = await createThread('HANDSHAKE_' + deviceId).catch(err =>
    log(`${FILENAME}# 222 #${err}`),
  );
  return threadId;
};

let getAddressByAccountIdx = async (phrase, idx) => {
  let wallet = await Textile.walletAccountAt(phrase, idx).catch(err =>
    log(`${FILENAME}# 229 #${err}`),
  );
  return wallet.address;
};

let updateDeviceList = async (deviceId, deviceAddress) => {
  let deviceListThreadId = STORE.getState().textileReducer.deviceListThreadId;
  let linkedMoment = moment().format('X');
  let payload = STORE.getState().deviceListReducer[deviceId];
  payload.deviceAddress = deviceAddress;
  payload.linkedMoment = linkedMoment;

  STORE.dispatch({
    type: types.UPDATE_DEVICE_IDX,
    payload: {
      deviceId,
      details: {
        deviceAddress,
        deviceLinked: true,
        linkedMoment: linkedMoment,
        fileCount: 0,
        disconnect: false,
      },
    },
  });

  const stringData = Buffer.from(JSON.stringify(payload)).toString('base64');
  await Textile.files
    .addData(stringData, deviceListThreadId, 'Added device ' + deviceId)
    .catch(err => log(`${FILENAME}# 258 #${err}`));
};

let getStartIdx = deviceList => {
  let maxIdx = 0;

  Object.keys(deviceList).map(
    d =>
      (maxIdx = deviceList[d].endIdx > maxIdx ? deviceList[d].endIdx : maxIdx),
  );

  return maxIdx;
};

let getContactQueryId = async address => {
  let query = ContactQuery.create();
  query.address = address;

  let options = QueryOptions.create();
  options.wait = 10;
  options.limit = 1;

  let contactSearchQueryId = await Textile.contacts
    .search(query, options)
    .catch(err => log(`${FILENAME}# 284 #${err}`));
  log(`${FILENAME}# 283 #Contact Search, ${contactSearchQueryId} , ${address}`);
  return contactSearchQueryId;
};

/* 
    To be called in 2 cases
    1. New message notification is recieved when app in foreground
    2. App is openned for the first time
*/
let updateNotifications = async () => {
  let devices = STORE.getState().deviceListReducer;
  /*  
        Ignores timedout notifications. In-time notifications(valid and with in time), 
        which have been responded-to still need to be in the list for tracking
     */
  let allNotifications = getValidNotifications(
    STORE.getState().notificationsReducer.valid,
  );
  let blockedNotifications = STORE.getState().notificationsReducer.blocked;
  let mobileAddress = await Textile.account
    .address()
    .catch(err => log(`${FILENAME}# 304 #${err}`));

  for (let d in devices) {
    let msgs = await Textile.messages
      .list('', 100, devices[d].notifyThreadId)
      .catch(err => log(`${FILENAME}# 309 #${err}`));

    msgs &&
      msgs.items &&
      msgs.items.map(m => {
        try {
          let n = JSON.parse(m.body);
          if (m.user.address == mobileAddress) {
            return;
          }

          if (n.action == 'DISCONNECT') {
            if (n.status == 'CANCEL') {
              STORE.dispatch({
                type: types.UPDATE_DEVICE,
                payload: {idx: d, details: {disconnect: false}},
              });
            } else if (n.status == 'CONFIRM') {
              deleteDevice(d, devices[d].notifyThreadId);
            }
            return;
          }
          // Valid notification and not already in the all Notifications list
          if (
            isValidNotifcation(n.from, n.till) &&
            allNotifications.every(note => note.msgBlock != m.block)
          ) {
            allNotifications.push({
              msgBlock: m.block,
              deviceName: devices[d].deviceName,
              notifyThreadId: devices[d].notifyThreadId,
              msg: {...n},
            });
          }
        } catch (err) {}
      });
  }

  allNotifications = allNotifications.sort((a, b) => a.msg.from - b.msg.from);
  // Retain blocked status only for valid messages
  blockedNotifications = blockedNotifications.filter(
    blockedNote =>
      !!allNotifications.find(validNote => blockedNote == validNote.msgBlock),
  );

  STORE.dispatch({
    type: types.UPDATE_NOTIFICATIONS_LIST,
    payload: {valid: allNotifications, blocked: blockedNotifications},
  });

  // If valid messages which have not been blocked
  if (
    getUnBlockedValidNotifications(allNotifications, blockedNotifications)
      .length > 0
  ) {
    //Show notification UI here
    NavigationService.navigate('NotificationScreen', {userName: 'Sekura'});
  }
};

let getUnBlockedValidNotifications = (
  validNotifications,
  blockedNotifications,
) => {
  return validNotifications.filter(validNote =>
    blockedNotifications.every(
      blockedNote => validNote.msgBlock != blockedNote,
    ),
  );
};

let getValidNotifications = notifications =>
  notifications.filter(note =>
    isValidNotifcation(note.msg.from, note.msg.till),
  );

let isValidNotifcation = (from, till) => {
  let current = parseInt(moment().format('X'));
  return current >= parseInt(from) && current <= parseInt(till);
};

let isValidActivity = msg => {
  try {
    msg = JSON.parse(msg);

    if (!!msg.type) {
      return true;
    }
  } catch (err) {}

  return false;
};

let deleteDevice = async (deviceAddress, notifyThreadId) => {
  let from = moment().format('X');
  let till = parseInt(from) + parseInt(settings.max_valid_notify_secs);
  let msg = {from, till, action: 'DISCONNECT', status: 'COMPLETE'};

  await Textile.messages
    .add(notifyThreadId, JSON.stringify(msg))
    .catch(err => log(`${FILENAME}# 409 #${err}`));

  STORE.dispatch({type: types.DELETE_DEVICE, payload: {idx: deviceAddress}});
};

/*** Actions  *****/

// msg = {from: [seconds since unix time], till: [seconds since unix time], action: 'LOCK/DISCONNECT'}
export function sendMessage(deviceAddress, notifyThreadId, msg) {
  return async dispatch => {
    await Textile.messages
      .add(notifyThreadId, JSON.stringify(msg))
      .catch(err => log(`${FILENAME}# 421 #${err}`));
    if (msg.action == 'DISCONNECT') {
      dispatch({
        type: types.UPDATE_DEVICE,
        payload: {idx: deviceAddress, details: {disconnect: true}},
      });
    }
  };
}

export function sendNotificationResponse(
  notifyThreadId,
  msgBlock,
  msg,
  response,
) {
  return async dispatch => {
    await Textile.messages
      .add(notifyThreadId, JSON.stringify({...msg, response}))
      .catch(err => log(`${FILENAME}# 440 #${err}`));
    dispatch({type: types.BLOCK_NOTFICATION, payload: msgBlock});
  };
}

export function getDeviceActivity(deviceAddress, offset) {
  return (dispatch, getState) => {
    let keyStoreThreadId = getState().deviceListReducer[deviceAddress]
      .keyStoreThreadId;
    dispatch({type: types.ASYNC_BUSY});

    if (offset) {
      Textile.messages
        .list(offset, 10, keyStoreThreadId)
        .then(msgs => {
          let log = [];
          msgs &&
            msgs.items &&
            msgs.items.map(m => {
              if (isValidActivity(m.body)) {
                let item = {
                  ...JSON.parse(m.body),
                  block: m.block,
                };
                log.push(item);
              }
            });
          dispatch({
            type: types.UPDATE_LOG,
            payload: {idx: deviceAddress, activity: log},
          });
          dispatch({type: types.ASYNC_DONE});
        })
        .catch(err => log(`${FILENAME}# 473 #${err}`));
    } else {
      Textile.messages
        .list('', 10, keyStoreThreadId)
        .then(msgs => {
          let log = [];
          msgs &&
            msgs.items &&
            msgs.items.map(m => {
              if (isValidActivity(m.body)) {
                let item = {
                  ...JSON.parse(m.body),
                  block: m.block,
                };
                log.push(item);
              }
            });
          dispatch({
            type: types.RESET_LOG,
            payload: {idx: deviceAddress, activity: log},
          });
          dispatch({type: types.ASYNC_DONE});
        })
        .catch(err => log(`${FILENAME}# 496 #${err}`));
    }
  };
}

export function setTextileStatus(payload) {
  return dispatch => {
    dispatch({type: types.SET_TEXTILE_STATUS, payload});
  };
}

export function launchTextile() {
  return async (dispatch, getState) => {
    try {
      await initAccount(dispatch).catch(err => log(`${FILENAME}# 510 #${err}`));
      await Textile.launch(repoPath, true).catch(err =>
        log(`${FILENAME}# 512 #${err}`),
      );
    } catch (err) {
      log(`${FILENAME}# 515 #${err}`);
      dispatch({type: types.SET_TEXTILE_STATUS, payload: false});
    }
  };
}

export function addDevice(deviceId, deviceName, deviceAddress, numOfNodes) {
  return async (dispatch, getState) => {
    dispatch({type: types.ASYNC_BUSY});
    log(
      `${FILENAME}# 525 #adding device - ${deviceId}, ${deviceName}, ${deviceAddress}, ${numOfNodes}`,
    );
    let phrase = getState().textileReducer.phrase;
    let startIdx = getStartIdx(getState().deviceListReducer) + 1;
    let endIdx = startIdx + (numOfNodes - 1);

    let handshakeThreadId = await createHandshakeThread(deviceId).catch(err =>
      log(`${FILENAME}# 532 #${err}`),
    );
    log(`${FILENAME}# 534 #Handshake Thread ID: ${handshakeThreadId}`);

    let contactQueryId = await getContactQueryId(deviceAddress).catch(err =>
      log(`${FILENAME}# 537 #${err}`),
    );
    log(`${FILENAME}# 539 #contactQueryId - ${contactQueryId}`);
    dispatch({
      type: types.ADD_DEVICE,
      payload: {
        deviceId,
        details: {
          deviceId,
          deviceName,
          deviceAddress,
          startIdx,
          endIdx,
          deviceLinked: false,
          fileCount: 0,
          linkedMoment: 0,
        },
      },
    });
    dispatch({type: types.ASYNC_DONE});

    emitter.on(contactQueryId, async () => {
      log(`${FILENAME}# 559 #received contactQueryId - ${contactQueryId}`);
      await Textile.invites
        .add(handshakeThreadId, deviceAddress)
        .catch(err => log(`${FILENAME}# 562 # ${err}`));
      log(`${FILENAME}# 563 #Desktop address: ${deviceAddress}`);

      log(`${FILENAME}# 565 #dispatching event`);
      // dispatch({
      //   type: types.ADD_DEVICE,
      //   payload: {
      //     deviceId,
      //     details: {
      //       deviceId,
      //       deviceName,
      //       deviceAddress,
      //       startIdx,
      //       endIdx,
      //       deviceLinked: false,
      //       fileCount: 0,
      //       linkedMoment: 0,
      //     },
      //   },
      // });
      // dispatch({type:types.ASYNC_DONE});
    });

    emitter.on(handshakeThreadId, async feedItem => {
      log(`${FILENAME}# 586 #Recieved handshake thread event: ${feedItem}`);

      if (
        feedItem.value &&
        feedItem.value.files &&
        feedItem.value.files.length > 0
      ) {
        let content = await Textile.files
          .content(feedItem.value.files[0].file.hash)
          .catch(err => log(`${FILENAME}# 595 #${err}`));
        let data = Buffer.from(content.data).toString();
        try {
          data = JSON.parse(data);
        } catch (err) {}
        log(`${FILENAME}# 600 #*** Thread data: , ${data}`);

        if (data.desktop_ack) {
          let mobileAddress = await Textile.account
            .address()
            .catch(err => log(`${FILENAME}# 605 #${err}`));
          deviceAddress = await getAddressByAccountIdx(phrase, startIdx).catch(
            err => log(`${FILENAME}# 607 #${err}`),
          );

          let fcmToken = getState().textileReducer.fcmToken;

          let deviceData = {
            mnemonic: phrase,
            start_index: startIdx,
            end_index: endIdx,
            mobile_address: mobileAddress,
            fcm_token: fcmToken,
          };
          let stringData = Buffer.from(JSON.stringify(deviceData)).toString(
            'base64',
          );
          await Textile.files
            .addData(stringData, handshakeThreadId, 'Init Handshake')
            .catch(err => log(`${FILENAME}# 624 #${err}`));
          updateDeviceList(deviceId, deviceAddress);
        }
      }
    });
  };
}
