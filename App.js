import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore, persistReducer } from 'redux-persist';
import reduxSetup from './src/lib/reduxSetup'
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Terms from './src/components/Terms'
import QRScanner from './src/components/QRScanner';
import DeviceDetail from './src/components/DeviceDetail';
import Settings from './src/components/Settings';
import HomeScreen from './src/components/HomeScreen';
import DeviceTest from './DeviceTest';
import Notification from './src/components/Notification';
import Splash from './src/components/Splash';
import QRTest from './src/components/QRTest';
import NavigationService from './src/lib/NavigationService';
import Logger from './src/components/Logger';

const MainNavigator = createStackNavigator({
    SplashScreen: {screen: Splash},
    TermsPage: {screen: Terms},
    HomePage: {screen: HomeScreen},
    QRScreen: {screen: QRScanner},
    DeviceDetailPage: {screen: DeviceDetail},
    SettingsPage: {screen: Settings},
    TestPage: {screen: DeviceTest},
    NotificationScreen: {screen: Notification},
    QRTestScreen: {screen: QRTest},
    LoggerScreen :{screen:Logger}
  },{
    initialRouteName: 'SplashScreen',
    headerMode: 'none'
  });
  
const AppContainer = createAppContainer(MainNavigator);

const App = () => {
  return (
    <Provider store={reduxSetup.store}>
        <PersistGate loading={null} persistor={reduxSetup.persistor}>
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </PersistGate>
    </Provider>
  );
};

export default App; 