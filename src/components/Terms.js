import React from 'react';
import {Platform} from 'react-native';
import {NeomorphBox} from 'react-native-neomorph-shadows';
import NavigationService from '../lib/NavigationService';
import DefaultPreference from 'react-native-default-preference';
import COLORS from '../resources/Colors';
import {ic_logo} from '../resources';
import {
  ScrollView,
  Dimensions,
  Image,
  TouchableOpacity,
  StyleSheet,
  setState,
} from 'react-native';
import {Container, Icon, Text} from 'native-base';

const {width, height} = Dimensions.get('window');
import {PERMISSIONS, request, check, RESULTS} from 'react-native-permissions';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
};

export default class Terms extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accepted: false,
    };
    this.onTermsSubmit = this.onTermsSubmit.bind(this);
  }

  onTermsSubmit() {
    DefaultPreference.set('agreed', 'yes').then(function() {
      console.log('done');
    });
    this.askForCameraPermission();
  }
  askForCameraPermission = () => {
    if (Platform.OS === 'android') {
      request(PERMISSIONS.ANDROID.CAMERA).then(result => {
        switch (result) {
          case RESULTS.GRANTED: {
            console.log('camera permission granted');
          }
          case RESULTS.DENIED: {
            console.log('camera permissions denied');
          }
          case RESULTS.BLOCKED: {
            console.log('camera permission blocked');
          }
        }
        this.askForStoragePermission();
      });
    }
    if (Platform.OS === 'ios') {
      request(PERMISSIONS.IOS.CAMERA).then(result => {
        switch (result) {
          case RESULTS.GRANTED: {
            console.log('camera permission granted');
          }
          case RESULTS.DENIED: {
            console.log('camera permissions denied');
          }
          case RESULTS.BLOCKED: {
            console.log('camera permission blocked');
          }
        }
        this.askForStoragePermission();
      });
    }
  };
  askForStoragePermission = () => {
    if (Platform.OS === 'android') {
      request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then(result => {
        switch (result) {
          case RESULTS.BLOCKED: {
            console.log('storage permission blocked');
          }
          case RESULTS.DENIED: {
            console.log('storage permission denied');
          }
          case RESULTS.GRANTED: {
            console.log('storage permission granted');
          }
        }
        NavigationService.navigate('HomePage', {userName: 'Sekura'});
      });
    }
    if (Platform.OS === 'ios') {
      request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(result => {
        switch (result) {
          case RESULTS.BLOCKED: {
            console.log('storage permission blocked');
          }
          case RESULTS.DENIED: {
            console.log('storage permission denied');
          }
          case RESULTS.GRANTED: {
            console.log('storage permission granted');
          }
        }
        NavigationService.navigate('HomePage', {userName: 'Sekura'});
      });
    }
  };

  render() {
    const {navigate} = this.props.navigation;

    return (
      <Container style={styles.background} accessibilityLabel="container_terms">
        <Image
          accessibilityLabel="logo_terms"
          style={styles.icLogo}
          source={ic_logo}
        />

        <Text accessibilityLabel="welcometext_terms" style={styles.welcomeText}>
          Welcome to Sekura
        </Text>

        <ScrollView
          accessibilityLabel="scrollview_terms"
          style={styles.tcContainer}
          onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              this.setState({
                accepted: true,
              });
            }
          }}>
          <Text style={styles.tcP} accessibilityLabel="scrollview_firsttext">
            Welcome to our website. If you continue to browse and use this
            website, you are agreeing to comply with and be bound by the
            following terms and conditions of use, which together with our
            privacy policy govern [business name]’s relationship with you in
            relation to this website. If you disagree with any part of these
            terms and conditions, please do not use our website.
          </Text>
          <Text style={styles.tcP}>
            The term ‘[business name]’ or ‘us’ or ‘we’ refers to the owner of
            the website whose registered office is [address]. Our company
            registration number is [company registration number and place of
            registration]. The term ‘you’ refers to the user or viewer of our
            website.
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} The content of the pages of this website is for your
            general information and use only. It is subject to change without
            notice.
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} This website uses cookies to monitor browsing
            preferences. If you do allow cookies to be used, the following
            personal information may be stored by us for use by third parties:
            [insert list of information].
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} Neither we nor any third parties provide any warranty or
            guarantee as to the accuracy, timeliness, performance, completeness
            or suitability of the information and materials found or offered on
            this website for any particular purpose. You acknowledge that such
            information and materials may contain inaccuracies or errors and we
            expressly exclude liability for any such inaccuracies or errors to
            the fullest extent permitted by law.
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} Your use of any information or materials on this website
            is entirely at your own risk, for which we shall not be liable. It
            shall be your own responsibility to ensure that any products,
            services or information available through this website meet your
            specific requirements.
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} This website contains material which is owned by or
            licensed to us. This material includes, but is not limited to, the
            design, layout, look, appearance and graphics. Reproduction is
            prohibited other than in accordance with the copyright notice, which
            forms part of these terms and conditions.
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} All trademarks reproduced in this website, which are not
            the property of, or licensed to the operator, are acknowledged on
            the website. Unauthorised use of this website may give rise to a
            claim for damages and/or be a criminal offence.
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} From time to time, this website may also include links to
            other websites. These links are provided for your convenience to
            provide further information. They do not signify that we endorse the
            website(s). We have no responsibility for the content of the linked
            website(s).
          </Text>
          <Text style={styles.tcL}>
            {'\u2022'} Your use of this website and any dispute arising out of
            such use of the website is subject to the laws of England, Northern
            Ireland, Scotland and Wales.
          </Text>
          <Text style={styles.tcP} accessibilityLabel="scrollview_lasttext">
            The use of this website is subject to the following terms of use
          </Text>
        </ScrollView>

        <TouchableOpacity
          accessible={true}
          accessibilityLabel="btn_terms_accept"
          onPress={this.onTermsSubmit}
          disabled={!this.state.accepted}>
          <NeomorphBox
            style={this.state.accepted ? styles.button : styles.buttonDisabled}>
            <Icon
              style={this.state.accepted ? styles.icon : styles.iconDisabled}
              name="home"
            />
            <Text
              style={
                this.state.accepted
                  ? styles.buttonText
                  : styles.buttonTextDisabled
              }>
              {' '}
              Accept & Continue{' '}
            </Text>
          </NeomorphBox>
        </TouchableOpacity>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    padding: 10,
    backgroundColor: COLORS.colorPrimary,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },

  icLogo: {
    width: 130,
    height: 130,
    alignSelf: 'center',
  },

  welcomeText: {
    marginTop: 30,
    alignSelf: 'center',
    color: COLORS.fontColorPrimary,
    fontSize: 26,
    fontFamily: 'ProductSans-Regular',
  },

  terms: {
    marginTop: 20,
    alignSelf: 'center',
    color: COLORS.fontColorPrimary,
    fontSize: 18,
    justifyContent: 'center',
    fontFamily: 'ProductSans-Regular',
  },

  tcP: {
    color: COLORS.fontColorPrimary,
    marginTop: 10,
    marginBottom: 20,
    fontSize: 14,
    fontFamily: 'ProductSans-Regular',
  },

  tcL: {
    color: COLORS.fontColorPrimary,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 14,
    fontFamily: 'ProductSans-Regular',
  },
  tcContainer: {
    color: COLORS.fontColorPrimary,
    backgroundColor: '#0077aa',
    marginTop: 15,
    marginBottom: 15,
    height: height * 0.7,
    padding: 5,
  },

  button: {
    shadowRadius: 3,
    borderRadius: 50,
    backgroundColor: COLORS.colorPrimary,
    width: 280,
    height: 50,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'ProductSans-Regular',
  },

  buttonDisabled: {
    opacity: 0.5,
    shadowRadius: 3,
    borderRadius: 50,
    backgroundColor: COLORS.colorPrimary,
    width: 280,
    height: 50,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'ProductSans-Regular',
  },

  icon: {
    color: COLORS.fontColorPrimary,
  },

  iconDisabled: {
    color: COLORS.fontColorPrimary,
  },

  buttonText: {
    color: COLORS.fontColorPrimary,
    fontSize: 18,
    marginLeft: 20,
    fontFamily: 'ProductSans-Regular',
  },

  buttonTextDisabled: {
    color: COLORS.fontColorPrimary,
    fontSize: 18,
    marginLeft: 20,
    fontFamily: 'ProductSans-Regular',
  },

  buttonLabel: {
    fontSize: 14,
    color: COLORS.fontColorPrimary,
    alignSelf: 'center',
  },
});
