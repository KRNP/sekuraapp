import * as types from '../actions/actionTypes';

export default function (state = false, action) {
    switch (action.type) {
        case types.ASYNC_BUSY:
            return true
        case types.ASYNC_DONE:
            return false
        default:
            return state;
    }
}
