import React from 'react';
import { Image, ImageBackground, TouchableOpacity, setState} from 'react-native';
import { StyleSheet, Alert, Share, Dimensions} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { ic_logo, background } from '../resources';
import { Container, Header, Left, Body, Right, Title, Textarea, Form, View, Text, Button, Icon, Fab } from 'native-base';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { NeomorphBox } from 'react-native-neomorph-shadows';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NavigationService from '../lib/NavigationService';
import * as acts from '../actions/actionCreator';

const { width , height } = Dimensions.get('window');

class QRTest extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            qrvalue:''
        };
        this.onSuccess = this.onSuccess.bind(this)
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    onChange = (value) => {
        this.setState({ qrvalue: value });
    }

    onSuccess() {

        let qrData = JSON.parse(this.state.qrvalue)

        var desktopPeerAddress = qrData.d_public_key
        var desktopDeviceId = qrData.d_device_id
        var desktopDeviceName = qrData.d_device_name
        var numOfNodes = qrData.num_nodes

        this.props.acts.addDevice(desktopDeviceId, desktopDeviceName, desktopPeerAddress, numOfNodes);

        this.props.navigation.goBack();
    }

    render() {
      const {navigate} = this.props.navigation;

      return (
        <Container accessibilityLabel='container_settings' style={styles.background}>
                
            <Form>
                <Textarea
                accessibilityLabel='edittext_code_qrtest'
                bordered 
                rowSpan={5}
                onChangeText={this.onChange}
                style={{backgroundColor:'white', width:width*0.9}}
                placeholder="Paste QR Code Here" />
            </Form>
            
            <TouchableOpacity accessibilityLabel='btn_code_qrtest' onPress={() => this.onSuccess()}>
                <NeomorphBox style={styles.buttonQRSubmit} >
                    <Text style={styles.textQRSubmit}> Connect Device </Text>
                </NeomorphBox>
            </TouchableOpacity>
                
        </Container>
      );
    }    
};

const mapDispatchToProps = dispatch => {
    return {
        acts: bindActionCreators(acts, dispatch)
    }
  };
  
  function mapStateToProps(state) {
    return {
        recoveryKey: state.textileReducer.phrase
    } 
  }

const styles = StyleSheet.create({

    background: {
        flex:1,
        backgroundColor:'#0088cc',
        flexDirection: 'column',
        alignItems:'center',
        justifyContent:'center'
    },

    buttonQRSubmit: {
        shadowRadius: 3,
        backgroundColor: '#0088cc',
        width: width * 0.9,
        height: 70,
        marginBottom:30,
        marginTop:20,
        flexDirection:'row',
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center'
    },

    textQRSubmit: {
        color:'white',
        fontSize:22,
        marginLeft:20
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(QRTest);